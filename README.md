# Alexander W. Rubin - Full Stack Developer

This is a portfolio site for:

Alexander W. Rubin\
104-20 Queens Blvd 11V\
Forest Hills, NY 11375

tel. - (347) 327-3356 \
email - alexanderwalterrubin@gmail.com

## Table of Contents

- [Installation](#installation)
- [Usage](#usage)
- [Contributing](#contributing)
- [License](#license)

## Installation

1. In your terminal, type (or copy/paste) the following: \
 git clone https://gitlab.com/AlexanderRubin/alexander-rubin.git

 2. In your terminal, type (or copy/paste): \
 cd alexander-rubin

 3. In your terminal, type (or copy/paste): \
 code .

 4. Once this project is open on VSCode or similar, right click on home.html, and click on copy path

 5. Paste the path into the browser to view this portfolio

## Usage

This portfolio is Read Only. Do not attempt to make edits to this repo.

## Contributing

Guidelines on how others can contribute to your project.

## License

Information about the license for your project.